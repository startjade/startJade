package protocols.consensus.paxos;


/**
 * @startuml ' TO_CHECK
 * title BasicPaxos
 * ' subprotocols {requestProtocol , ProposeProtocol , PromiseProtocol , Nak , AcceptProtocol,DecideProtocol,AnswerProtocol}
 * ' rolemode true
 * group parallel-repeat {2,2} once for each client
 * loop {1,2} Don't know why but sometimes each consensus is made twice
	 * loop {1,} Client ask all Proposers
		 * Client -> Proposer : REQUEST
	 * end loop
	 * loop {1,} While proposers and acceptors haven't decided on the value to choose
 		* alt Rejections
 			* Proposer -> Acceptor : REQUEST
			* Acceptor -> Proposer : REJECT_PROPOSAL
		* else
			* Proposer -> Acceptor : REQUEST
			* Acceptor -> Proposer : ACCEPT_PROPOSAL
 		* end alt
 	 * end loop
	 * loop {1,} While proposers and acceptors haven't decided on the value to choose
		* alt Rejections
			* Proposer -> Acceptor : REQUEST
			* Acceptor -> Proposer : REJECT_PROPOSAL
		* else At least a majority of accepts (here, total of 4 acceptors)
			* Proposer -> Acceptor : REQUEST
			* Acceptor -> Proposer : ACCEPT_PROPOSAL
		* end alt
	 * end loop
 	 * group parallel-repeat {2,}  Learning phase : Proposers tells learner about the value, learner tells the res to clients
 	 	* Proposer -> Learner : INFORM
 		* Learner -> Client : INFORM
	 * end parallel-repeat
 * end loop
 * end parallel-repeat
 * note across : Possible end
 * @enduml
*/

/**
 * 
 * @startuml
 * Client->>Proposer: request(topic, val1)
 * Proposer->>Acceptor1:Propose(topic, 1)
 * note over Acceptor1: The first Proposer is always accepted.
 * Proposer->>Acceptor2:Propose(topic, 1)
 * note over Acceptor2: The first Proposer is always accepted.
 * Acceptor2->>Proposer:Promise(topic, 1, null)
 * Acceptor1->>Proposer:Promise(topic, 1, null)
 * Proposer->>Acceptor1:Accept(topic, 1, val)
 * Proposer->>Acceptor2:Accept(topic, 1, val)
 * Acceptor2->>Proposer:Accepted(topic, 1, val)    
 * Acceptor1->>Proposer:Accepted(topic, 1, val)
 * Proposer->>Leaner:Decide(topic, val)
 * Leaner->>Client:res
 * note over Client: res is the answer of the request(topic, val).
 * @enduml
 * 
 * 
 *
 */
/**
 * This class is used to implement the Basic Paxos consensus.
 * 
 * @author axel foltyn
 *
 */
public class BasicPaxos extends Paxos {

	public BasicPaxos() {
		super(PaxosImplemented.BasicPaxos);
	}

}
