package protocols;

public enum Protocol {
	EnglishAuction, JapaneseAuction, DutchAuction, ReverseEnglishAuction, ReverseJapaneseAuction, ReverseDutchAuction, LNS, PushSum,
	BasicPaxos, MultiPaxos
}
