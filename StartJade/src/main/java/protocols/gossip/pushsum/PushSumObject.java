package protocols.gossip.pushsum;

public class PushSumObject {
	private boolean stop;

	public PushSumObject(boolean stop) {
		this.stop = stop;
	}

	public boolean isStop() {
		return stop;
	}

	public void setStop(boolean stop) {
		this.stop = stop;
	} 

	
}

